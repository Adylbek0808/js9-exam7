import React, { useState } from 'react';
import './App.css';
import { Container, Grid } from '@material-ui/core';
import hamburgerImage from '../../MenuImages/hamburger.svg';
import cheesburgerImage from '../../MenuImages/cheeseburger.svg';
import friesImage from '../../MenuImages/fries.svg';
import coffeeImage from '../../MenuImages/coffee.svg';
import teaImage from '../../MenuImages/tea.svg';
import colaImage from '../../MenuImages/soft-drink.svg';
import { nanoid } from 'nanoid';
import FoodMenuList from '../../components/FoodMenuList/FoodMenuList';
import OrderDetailList from '../../components/OrderDetailList/OrderDetailList';

const MENUITEMS = [
  { id: nanoid(), name: "Hamburger", price: 80, image: hamburgerImage },
  { id: nanoid(), name: "Cheeseburger", price: 90, image: cheesburgerImage },
  { id: nanoid(), name: "Fries", price: 45, image: friesImage },
  { id: nanoid(), name: "Coffee", price: 70, image: coffeeImage },
  { id: nanoid(), name: "Tea", price: 50, image: teaImage },
  { id: nanoid(), name: "Cola", price: 40, image: colaImage }
];

const App = () => {
  const [orderDetail, setOrderDetail] = useState(MENUITEMS.map(item => {
    return (
      { id: item.id, name: item.name, count: 0, price: item.price }
    );
  }));

  const addMenuItem = id => {
    const index = orderDetail.findIndex(item => item.id === id);
    const itemCopy = orderDetail[index];
    itemCopy.count++
    const orderDetailCopy = [...orderDetail];
    orderDetailCopy[index] = itemCopy;
    setOrderDetail(orderDetailCopy);
  };

  const removeMenuItem = id => {
    const index = orderDetail.findIndex(item => item.id === id);
    const itemCopy = orderDetail[index];
    if (itemCopy.count > 0) {
      itemCopy.count--
      const orderDetailCopy = [...orderDetail];
      orderDetailCopy[index] = itemCopy;
      setOrderDetail(orderDetailCopy);
    }
  };

  const itemsOrdered = orderDetail.filter(item => {
    return item.count > 0
  });

  const getTotalPrice = orderDetail.reduce((acc, item) => {
    acc = acc + (item.count * item.price)
    return acc
  }, 0);

  let orderMessage;

  if (getTotalPrice === 0) {
    orderMessage = "Your Order is Empty! \n Please select some items from the menu!"
  } else {
    orderMessage = "Total cost: " + getTotalPrice + " KGS"
  }

  return (
    <Container className="App">
      <h1>Order interface</h1>
      <Grid container>
        <Grid item xs={4}>
          <h3>Order details:</h3>
          <Grid container spacing={1}>
            <OrderDetailList orderList={itemsOrdered} removeItem={removeMenuItem}></OrderDetailList>
            <Grid item xs={12}>
              <p className="TotalCost">
                {orderMessage}
              </p>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={8}>
          <h3>Add Items:</h3>
          <Grid container spacing={1}>

            <FoodMenuList foodMenu={MENUITEMS} addItem={addMenuItem} />
          </Grid>

        </Grid>
      </Grid>
    </Container>
  );
}

export default App;
