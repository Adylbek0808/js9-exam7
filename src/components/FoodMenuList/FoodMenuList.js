import React from 'react';
import FoodMenuItem from '../FoodMenuItem/FoodMenuItem';

const FoodMenuList = props => {
    return props.foodMenu.map(item => {
        return <FoodMenuItem
            key={item.id}
            image={item.image}
            name={item.name}
            price={item.price}
            addItem={() => props.addItem(item.id)}
        />
    });
};

export default FoodMenuList;