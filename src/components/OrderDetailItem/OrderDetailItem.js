import React from 'react';
import { Grid, Button } from '@material-ui/core';

const OrderDetailItem = props => {
    const itemTotalPrice = props.count * props.price;
    return (
        <Grid item xs={12}>
            <Grid container alignItems="center">
                <Grid item xs={4}><p>{props.name}</p></Grid>
                <Grid item xs={1}><p>x{props.count}</p></Grid>
                <Grid item xs={3}><p>{itemTotalPrice} KGS</p></Grid>
                <Grid item xs={3}>
                    <Button variant="contained" onClick={props.removeItem}>REMOVE</Button>
                </Grid>
            </Grid>
        </Grid>
    )
};
export default OrderDetailItem;
