import React from 'react';
import OrderDetailItem from '../OrderDetailItem/OrderDetailItem';

const OrderDetailList = props => {   
    return props.orderList.map(item => {
        return <OrderDetailItem
            key={item.id}
            name={item.name}
            count={item.count}
            price={item.price}
            removeItem={() => props.removeItem(item.id)}
        />
    })
};
export default OrderDetailList;