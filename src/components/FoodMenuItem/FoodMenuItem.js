import React from 'react';
import './FoodMenuItem.css';
import { Paper, Grid } from '@material-ui/core';

const FoodMenuItem = props => {
    return (
        <Grid item xs={6} onClick={props.addItem}>
            <Paper elevation={3}>
                <Grid container alignItems="center">
                    <Grid item xs={2}>
                        <img src={props.image} className="Icon" alt="" ></img>
                    </Grid>
                    <Grid item xs={4}>
                        <p className="Name">{props.name}</p>
                        <p className="Price">Price: {props.price} KGS</p>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    )
};
export default FoodMenuItem;